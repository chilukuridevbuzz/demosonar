package com.sree.springsample.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SpringSampleController {
	private static final Logger logger = LoggerFactory.getLogger(SpringSampleController.class);
	Double result;
	String validationMessage;
	private static final String NO_PARAMS_MESSAGE = "Pameters cannot be empty";
	private static final String ONE_PARAM_MISSING_MESSAGE = "Parameter %s cannot be empty";

	@GetMapping(path = "/welcome")
	@ResponseBody
	public String home() {
		return "Sample Application welcomes";
	}

	@GetMapping(path = "/add")
	@ResponseBody
	public String add(@RequestParam(value = "val1", required = false) Double val1,
			@RequestParam(value = "val2", required = false) Double val2, Model model) {

		String inputValidationResult = validateInputs(val1, val2);
		if (inputValidationResult.equals("")) {
			result = (val1 + val2);
			validationMessage = "" + result;
		} else {
			return inputValidationResult;
		}

		return validationMessage;
	}

	@GetMapping(path = "/sub")
	@ResponseBody
	public String sub(@RequestParam(value = "val1", required = false) Double val1,
			@RequestParam(value = "val2", required = false) Double val2, Model model) {
		String inputValidationResult = validateInputs(val1, val2);
		if (inputValidationResult.equals("")) {
			result = (val1 - val2);
			validationMessage = "" + result;
		} else {
			return inputValidationResult;
		}

		return validationMessage;
	}

	@GetMapping(path = "/mul")
	@ResponseBody
	public String mul(@RequestParam(value = "val1", required = false) Double val1,
			@RequestParam(value = "val2", required = false) Double val2, Model model) {
		String inputValidationResult = validateInputs(val1, val2);
		if (inputValidationResult.equals("")) {
			result = (val1 * val2);
			validationMessage = "" + result;
		} else {
			return inputValidationResult;
		}

		return validationMessage;
	}

	@GetMapping(path = "/div")
	// @RequestMapping(value="/div", method = RequestMethod.GET)
	@ResponseBody
	public String div(@RequestParam(value = "val1", required = false) Double val1,
			@RequestParam(value = "val2", required = false) Double val2, Model model) {
		String inputValidationResult = validateInputs(val1, val2);
		if (inputValidationResult.equals("")) {
			result = (val1 / val2);
			validationMessage = "" + result;
		} else {
			return inputValidationResult;
		}

		return validationMessage;
	}

	public String validateInputs(Double val1, Double val2) {
		String resultVal = "";
		if (val1 == null) {
			resultVal = "val1";
			try {
				throw new com.sree.springsample.InvalidArgumentExpection("Parameter val1 cannot be null");
			} catch (com.sree.springsample.InvalidArgumentExpection e) {
				logger.error("InvalidArgumentExpection caught");
			} catch (Exception e) {
				logger.error("expection caught wrt val1");
			}
		}

		if (val2 == null) {

			resultVal = (resultVal.equals("")) ? "val2" : "|val2";
			try {
				throw new com.sree.springsample.InvalidArgumentExpection("Parameter val2 cannot be null");
			} catch (com.sree.springsample.InvalidArgumentExpection e) {
				logger.error("InvalidArgumentExpection caught");
			} catch (Exception e) {
				logger.error("expection caught wrt val2");
			}
		}

		if (resultVal.equals("")) {
			return resultVal;

		} else {
			validationMessage = resultVal.contains("|") ? NO_PARAMS_MESSAGE : String.format(ONE_PARAM_MISSING_MESSAGE, resultVal);
		}
		return validationMessage;
	}

}
