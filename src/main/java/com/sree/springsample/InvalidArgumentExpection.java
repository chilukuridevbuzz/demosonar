package com.sree.springsample;

@SuppressWarnings("serial")
public class InvalidArgumentExpection extends Exception {
	public InvalidArgumentExpection(String s) 
    { 
        // Call constructor of parent Exception 
        super(s);
    }
}
