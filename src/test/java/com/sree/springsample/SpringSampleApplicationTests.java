package com.sree.springsample;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.sree.springsample.controller.SpringSampleController;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SpringSampleApplicationTests {

	@Test
	public void greetMessage() {

		SpringSampleController abc = new SpringSampleController();
		String expected = "Sample Application welcomes";
		assertEquals(expected, abc.home());
	}

	@Test
	public void addValuesOneNull() {

		Double num1 = (double) 10;

		SpringSampleController abc = new SpringSampleController();
		String res = abc.add(num1, null, null);
		// assertEquals(30, res);

		assertEquals("Parameter val2 cannot be empty", res);
	}

	@Test
	public void addValuesTwoNull() {

		Double num1 = (double) 10;

		SpringSampleController abc = new SpringSampleController();
		String res = abc.add(null, num1, null);
		// assertEquals(30, res);

		assertEquals("Parameter val1 cannot be empty", res);
	}

	@SuppressWarnings("unused")
	@Test
	public void addValuesBothNull() {

		Double num1 = (double) 10;

		SpringSampleController abc = new SpringSampleController();
		String res = abc.add(null, null, null);
		// assertEquals(30, res);

		assertEquals("Pameters cannot be empty", res);
	}

	@Test
	public void addValues() {

		Double num1 = (double) 10;
		Double num2 = (double) 20;

		SpringSampleController abc = new SpringSampleController();
		String res = abc.add(num1, num2, null);
		// assertEquals(30, res);
		assertEquals(Double.valueOf(30), Double.valueOf(res));
	}

	@Test
	public void subValues() {

		Double num1 = (double) 50;
		Double num2 = (double) 10;

		SpringSampleController abc = new SpringSampleController();
		String res = abc.sub(num1, num2, null);
		// assertEquals(30, res);
		assertEquals(Double.valueOf(40), Double.valueOf(res));
	}

	@Test
	public void mulValues() {

		Double num1 = (double) 50;
		Double num2 = (double) 10;

		SpringSampleController abc = new SpringSampleController();
		String res = abc.mul(num1, num2, null);
		// assertEquals(30, res);
		assertEquals(Double.valueOf(500), Double.valueOf(res));
	}

	@Test
	public void divValues() {

		Double num1 = (double) 50;
		Double num2 = (double) 10;

		SpringSampleController abc = new SpringSampleController();
		String res = abc.div(num1, num2, null);
		// assertEquals(30, res);
		assertEquals(Double.valueOf(5), Double.valueOf(res));
	}
}
